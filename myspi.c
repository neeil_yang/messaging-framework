#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <sys/types.h>
#include <stdint.h>

#include "bcm2835.h"



//SPI interface needs to be enabled to use hardware spi interface
//on the specified pins

//spi is a forced full duplex protocol, data is sent and received simulateneously between master and slave
//


//could just make it return what was recieved. don't need to pass pointer
// void concrete_transfer(uint8_t send_data, uint8_t *recv_buffer, int slave_address) {

// }

//send the empty code if only interested in reading
//slave acknowledges that it didn't recieve anything useful

//never want to write and ignore what the slave has prepared.
//if it has nothing important to say it should be sending the emoty code
//so we don't waste space or think
//this may only be a problem if a slave can prepare data to be sent
//before being prompted to send data
//#define EMPTY_CODE 0

//need to define code words


enum Slave{slave1, slave2};


//can these be combined or is it only one at a time...i.e can I combine to get 4 values
//might have to modify the library
void address_slave(enum Slave slave_address) {
    switch (slave_address)
    {
    case slave1:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
        break;
    case slave2:
        bcm2835_spi_chipSelect(BCM2835_SPI_CS1);
        break;
    default:
        perror("Invalid Slave");
        break;
    }
}

void initialize_spi(void) {
    assert(bcm2835_init()); //gain access to physical memory...peripheral registers essentially
    assert(bcm2835_spi_begin()); //enable spi pins instead of gpio

    //is this going to result in little endian data or little endian data
    bcm2835_spi_setBitOrder(BCM2835_SPI_BIT_ORDER_MSBFIRST); //does this mean sending the most significant bit first
    
    //default mode
    //need to match polarity and phase with slave
    //will this need to be set for each individual slave you communicate with,
    //i.e lookup table and reset with each transfer
    bcm2835_spi_setDataMode(BCM2835_SPI_MODE0);
    bcm2835_spi_setClockDivider(BCM2835_SPI_CLOCK_DIVIDER_65536); //default clock speed...hz given in pi3, what about zero..may be too fast..may need to change
    //bcm2835_spi_chipSelect(BCM2835_SPI_CS0);
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS0, LOW); //active when pin low
    //bcm2835_spi_chipSelect(BCM2835_SPI_CS1); //select the pin on transfer
    bcm2835_spi_setChipSelectPolarity(BCM2835_SPI_CS1, LOW); //need to coordinate the active high or low of slave devices
}

//sends data to slave and returns what was recieved from slave
uint8_t concrete_transfer_byte(uint8_t send_data, enum Slave slave) {
    address_slave(slave);
    bcm2835_spi_transfer(send_data);
}

void concrete_transfer_buffer(char *send_buf, char *recv_buf, uint32_t buf_len, enum Slave slave) {
    address_slave(slave);
    bcm2835_spi_transfernb(send_buf, recv_buf, buf_len);
}

//sends data to slave ignoring revieved data
//data ready to be recieved will be lost
void concrete_send(uint8_t send_data, int slave_address) {
    concrete_transfer(send_data, slave_address);
}

//data ready to be sent will be lost
uint8_t concrete_recieve(int slave_address) {
    return concrete_transfer(0, slave_address);
}



int main(int argc, char **argv){
    //testing stuff
}