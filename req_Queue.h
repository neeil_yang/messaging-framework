/*
 * req_Queue.h
 */

/*
 * global queue for requests
 */
struct req_Queue *queue;

/*
 * FIFO Linked list for queueing requests
 * 		Global data structure?
 */
struct req_Queue {
	// other metadata
	struct req_Node head;
	struct req_Node tail;
	int count;
};

/*
 * Node containing data and metadata
 */
struct req_Node {
	// other metadata
	char ** data; 			// pointer to a string of data or whatver it is
	struct req_Node next;
};


/*
 * Add a request onto request queue
 */
void add_Request (char ** input_Data);

/*
 * Pop a request from request queue
 */
struct req_Node pop_Request (void);

/*
 * Create a request queue
 */
struct req_Queue new_Queue (void);

/*
 * Create a request node
 */
struct req_Node new_Node (char ** input_Data);

/*
 * Delete global request queue
 */
void del_Queue (struct req_Queue queue);

/*
 * Delete a request node
 */
void del_Node (struct req_Node request);
