//does parity bit in most significant or least significant postion matter.
#define PACKET_LENGTH 8
#define TRAILING_BIT 0x1
#define INFO_BITS 7

char buffer[256];


// Set parity bit
void setParity(unsigned char b) {
    int ones = countInfoOnes(b);
    b = b | ((ones % 2) << 7);
    return b;
}

int countInfoOnes(unsigned char b) {
    int ones = 0;
    for (int i = 0; i < 7; i++) {
        if (b & (1 << i)):           // If rightmost bit set
            ones += 1;
    }
}

// 2 bytes -> 16  info bits
// floor(16/3) 5 + 1r
// floor(16/4) = 4

// int countBitOnes(unsigned char b) {
//     unsigned char flag = 0x1;
//     int tally = 0;
//     for (int i = 0; i < sizeof(b) -1; i++) {
//         if ((b & flag) != 0) tally++;
//         flag  = flag << 1;
//     }
//     return tally;
// }



//offset from 0-7 (inclusive)
//leading bit for parity bit
//
unsigned char resolveBytes(unsigned char *message, int staringByte, unsigned char offset) {
    //if offset > 1 then need to straddle otherwise don't
    unsigned char window;
    if (offset > 1) {
        //00111111
        //initial grab = 7-offset +1
        //remaining grab = 7 - initial grab = offset -1

        window = message[staringByte];
        window = window << (offset -1);
        unsigned char window2 = message[staringByte + 1];
        window2 = window2 >> (7 - (offset - 2));
        unsigned char remainingFlag = 0xff >> (7 - (offset - 2));
        window2 &= remainingFlag;
        window |= window2; 
    }
    else {
        window = message[startingByte];
        if (offset == 0 {
            window = window >> 1;
        }
    }
    return window;
}
//message = 00110101....
//message = 'S1'
//message = 'S2'

// 0b_1111_111P
// 0b_1111_111P

// 0b_P111_1111 0b_P111_1111

//'HELLO WORLD'

//grab 7
//grab 6
//grab 5



//grab 7

//might be better if more evenly distributed --> i suspect it might be better
//char *message ----> bit stream
//return an array of bytes of error correcting packets
unsigned char *errorPacketize(unsigned char *message, int byteLen, int bitLen, int *n_packets) {
    int buffIndex = 0;
    unsigned char savedBit;
    unsigned char nextBit;
    unsigned char window;
    unsigned char packet;
    int startingByte = 0;
    unsigned char offset = 0;
    while (byte < byteLen) {
        window = message[byte];
        savedBit = window & TRAILING_BIT;
        packet = window >> 1;
        int ones = countBitOnes(packet);
        if (ones % 2 == 1) {
            char leading = TRAILING_BIT << 7;
            packet |= leading;
        }
        buffer[buffIndex] = packet;
        nextBit = savedBit;
    }
    

    return (char **)&buffer;
}

unsigned char **errorUnpack(unsigned char **packets) {
    e
}