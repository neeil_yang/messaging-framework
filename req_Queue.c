#include "req_Queue.h"

/*
Questions:
	Errno checks? (errnos and checking fn return / malloc vals)
	How are we planning to handle errors?

Other things to maybe implement:
	seperate queues for high priority and low priority requests?
*/

/*
 * Add a request onto request queue
 */
void add_Request (char ** input_Data) {
	struct request = new_Node(input_Data);
	if (queue->count == 0) {
		queue->head = request;
	} else {
		queue->tail->next = request;
	}
	queue->tail = request;
	queue->count++;
}

/*
 * Pop a request from request queue
 */
struct req_Node pop_Request (void) {
	struct req_Node request = NULL;
	if (queue->count != 0) {
		request = queue->head;
		queue->head = request->next;
		queue->count--;
		if (queue->count == 0) {
			queue->tail = NULL;
		}
	}
	return request;
}

/*
 * Create a request queue
 */
struct req_Queue new_Queue (void) {
	queue = malloc(sizeof struct req_Queue);
	queue->head = NULL;
	queue->tail = NULL;
	return queue;
}

/*
 * Create a request node
 */
struct req_Node new_Node (char ** input_Data) {
	struct req_Node request = malloc(sizeof struct req_Node);
	request->data = input_Data;
	request->next = NULL;
	return request;
}

/*
 * Delete request queue
 */
void del_Queue (struct req_Queue queue) {
	free(queue);
}

/*
 * Delete a request node
 */
void del_Node (struct req_Node request) {
	if (request != NULL) {
		free(request->data);		
	}
	free(request);
}
