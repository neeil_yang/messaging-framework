#include <stdio.h>
#include "req_Queue.c"

//MOCK systems
#define SUB_0 0x1
#define SUB_1 0x2

//MOCK signals
#define READ 0x1
#define WRITE 0x2






//PINS / SUBSYSTEMS

#define CONTROL 
#define BATTERY
#define ADCS
#define COMMS

//control signals
//power control signals
#define TOGGLE_ON
#define TOGGLE_OFF

//subsystem addressing
#define POW_ADCS
#define POW_COMMS
//...


#define A
#define B
#define C


char memory[256];
char size;

struct node {
    struct node *next;
    char command;
    char subsystemAddr;
    char *data;
};

struct queue {
    struct node *head;
    struct node *tail;
    int size;
};


int powerCommand(int command, int subsystem) {
    int newCommand = packageCommand(command, subsystem);
    sendCommand(CONTROL, newCommand);
}

//returns appropriate bit pattern
int packageCommand(int command, int subsystem) {
    return command & subsystem;
}

int func(int a, int b) {

}

int loop1() {
    while(1) {
        //data retrieval
        checkComms();
        getPowerData();
        //error checking
        ErrorChecking();
        //actions
    }
}

int loop2() {
    while(1) {
        commsCycle();
        ADCSCycle();
        batteryCycle();
        powerCycle();
    }
}

int main(void) {
    struct queue *request = newQueue();
    //add operations...i.e some nodes
    while(1) {
        //2 endpoints
        //check both endpoints for data
        //
        readEndpoint("dataA", request);
        readEndpoint("dataB", request);
        //send data to endpoints based on requests
        //loop through request queue
        while(requests) {
            //pop request
            struct node *request = popRequest();
            if (request->command == WRITE) {
                sendData(node->subsystemAddr, node->data);
            }
            else if (request->command == READ) {
                char data readData(node->subsystemAddr);
                memory[size] = data;
                size++;
            }
            
        }
    }
}

void readEndpoint(char *filename, struct queue *request) {
    FILE *f;

    f = fopen(filename, "r");
    char buff[255];

    int c = fgetc(f);
    while (c != EOF) {
        // Add byte from file into queue
        add_Request(c);
        c = fgetc(f);
    }

    fclose(f);
}